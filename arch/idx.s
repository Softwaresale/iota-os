
    .extern isr_handler
    .extern irq_handler

isr_common_stub:
    pusha
    mov %ax, %ds
    push %eax
    mov $0x10, %ax
    mov %ax,   %ds
    mov %ax,   %es
    mov %ax,   %fs
    mov %ax,   %gs

    call isr_handler

    /* restore state */
    pop %eax
    mov %ax, %ds
    mov %ax, %es
    mov %ax, %fs
    mov %ax, %gs
    popa
    add $8, %esp
    sti
    iret

irq_common_stub:
    pusha
    mov %ds, %ax
    push %eax
    mov $0x10, %ax
    mov %ax, %ds
    mov %ax, %es
    mov %ax, %fs
    mov %ax, %gs

    call irq_handler

    pop %ebx
    mov %bx, %ds
    mov %bx, %es
    mov %bx, %fs
    mov %bx, %gs
    popa
    add $8, %esp
    sti
    iret

    .global isr_zero_divide
    .global isr_debug
    .global isr_non_mask_int
    .global isr_int3
    .global isr_into
    .global isr_out_bounds
    .global isr_invalid_opcode
    .global isr_coproc_not_available
    .global isr_double_fault
    .global isr_coproc_seg_overrun
    .global isr_bad_tss
    .global isr_no_seg
    .global isr_stack_fault
    .global isr_gen_protect
    .global isr_page_fault
    .global isr_res15
    .global isr_float_pt
    .global isr_align_check
    .global isr_machine_check
    .global isr_res19
    .global isr_res20
    .global isr_res21
    .global isr_res21
    .global isr_res22
    .global isr_res23
    .global isr_res24
    .global isr_res25
    .global isr_res26
    .global isr_res27
    .global isr_res28
    .global isr_res29
    .global isr_res30
	.global isr_res31

    .global irq0
    .global irq1
    .global irq2
    .global irq3
    .global irq4
    .global irq5
    .global irq6
    .global irq7
    .global irq8
    .global irq9
    .global irq10
    .global irq11
    .global irq12
    .global irq13
    .global irq14
    .global irq15



isr_zero_divide:
    cli
    pushl $0
    pushl $0
    jmp isr_common_stub

isr_debug:
    cli
    pushl $0
    pushl $1
    jmp isr_common_stub

isr_non_mask_int:
    cli
    pushl $0
    pushl $2
    jmp isr_common_stub

isr_int3:
    cli
    pushl $0
    pushl $3
    jmp isr_common_stub

isr_into:
    cli
    pushl $0
    pushl $4
    jmp isr_common_stub


isr_out_bounds:
    cli
    pushl $0
    pushl $5
    jmp isr_common_stub

isr_invalid_opcode:
	cli
    pushl $0
    pushl $6
    jmp isr_common_stub

isr_coproc_not_available:
    cli
    pushl $0
    pushl $7
    jmp isr_common_stub

isr_double_fault:
    cli
    pushl $8
    jmp isr_common_stub

isr_coproc_seg_overrun:
    cli
	pushl $0
    pushl $9
	jmp isr_common_stub

isr_bad_tss:
    cli
	pushl $10
	jmp isr_common_stub

isr_no_seg:
    cli
    pushl $11
    jmp isr_common_stub

isr_stack_fault:
    cli
    pushl $12
    jmp isr_common_stub

isr_gen_protect:
    cli
	pushl $13
	jmp isr_common_stub

isr_page_fault:
    cli
	pushl $14
	jmp isr_common_stub

isr_res15:
    cli
	pushl $0
	pushl $15
	jmp isr_common_stub

isr_float_pt:
    cli
	pushl $0
	pushl $16
	jmp isr_common_stub

isr_align_check:
    cli
	pushl $0
	pushl $17
	jmp isr_common_stub

isr_machine_check:
    cli
	pushl $0
	pushl $18
	jmp isr_common_stub

isr_res19:
    cli
	pushl $0
	pushl $19
	jmp isr_common_stub

isr_res20:
    cli
	pushl $0
	pushl $20
	jmp isr_common_stub

isr_res21:
    cli
	pushl $0
	pushl $21
	jmp isr_common_stub

isr_res22:
    cli
	pushl $0
	pushl $22
	jmp isr_common_stub

isr_res23:
    cli
	pushl $0
	pushl $23
	jmp isr_common_stub

isr_res24:
    cli
	pushl $0
	pushl $24
	jmp isr_common_stub

isr_res25:
    cli
	pushl $0
	pushl $25
	jmp isr_common_stub

isr_res26:
    cli
	pushl $0
	pushl $26
	jmp isr_common_stub

isr_res27:
    cli
	pushl $0
	pushl $27
	jmp isr_common_stub

isr_res28:
    cli
	pushl $0
	pushl $28
	jmp isr_common_stub

isr_res29:
    cli
	pushl $0
	pushl $29
	jmp isr_common_stub

isr_res30:
    cli
	pushl $0
	pushl $30
	jmp isr_common_stub

isr_res31:
    cli
	pushl $0
	pushl $31
	jmp isr_common_stub

    /* IRQ */

irq0:
    cli
    pushl $0
    pushl $32
    jmp irq_common_stub

irq1:
    cli
    pushl $1
    pushl $33
    jmp irq_common_stub

irq2:
    cli
    pushl $2
    pushl $34
    jmp irq_common_stub

irq3:
    cli
    pushl $3
    pushl $35
    jmp irq_common_stub


irq4:
    cli
    pushl $4
    pushl $36
    jmp irq_common_stub

irq5:
    cli
    pushl $5
    pushl $37
    jmp irq_common_stub

irq6:
	cli
	pushl $6
	pushl $38
	jmp irq_common_stub


irq7:
	cli
	pushl $7
	pushl $39
	jmp irq_common_stub

irq8:
    cli
    pushl $8
    pushl $40
    jmp irq_common_stub

irq9:
    cli
    pushl $9
    pushl $41
    jmp irq_common_stub

irq10:
    cli
    pushl $10
    pushl $42
    jmp irq_common_stub

irq11:
    cli
    pushl $11
    pushl $43
    jmp irq_common_stub

irq12:
    cli
    pushl $12
    pushl $44
    jmp irq_common_stub

irq13:
    cli
    pushl $13
    pushl $45
    jmp irq_common_stub

irq14:
    cli
    pushl $14
    pushl $46
    jmp irq_common_stub

irq15:
    cli
    pushl $15
    pushl $47
    jmp irq_common_stub
