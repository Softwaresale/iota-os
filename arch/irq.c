
#include <pic.h>
#include <stdint.h>
#include <irq.h>
#include <isr.h>
#include <idt.h>

void irq_init()
{
        // remap the PIC
        pic_remap(0x11, 0x11);
        pic_remap(0x20, 0x28);
        pic_remap(0x04, 0x02);
        pic_remap(0x01, 0x01);
        pic_remap(0x00, 0x00);

        // install IRQs
        idt_set_gate(IRQ0,  (uint32_t) irq0);
        idt_set_gate(IRQ1,  (uint32_t) irq1);
        idt_set_gate(IRQ2,  (uint32_t) irq2);
        idt_set_gate(IRQ3,  (uint32_t) irq3);
        idt_set_gate(IRQ4,  (uint32_t) irq4);
        idt_set_gate(IRQ5,  (uint32_t) irq5);
        idt_set_gate(IRQ6,  (uint32_t) irq6);
        idt_set_gate(IRQ7,  (uint32_t) irq7);
        idt_set_gate(IRQ8,  (uint32_t) irq8);
        idt_set_gate(IRQ9,  (uint32_t) irq9);
        idt_set_gate(IRQ10, (uint32_t) irq10);
        idt_set_gate(IRQ11, (uint32_t) irq11);
        idt_set_gate(IRQ12, (uint32_t) irq12);
        idt_set_gate(IRQ13, (uint32_t) irq13);
        idt_set_gate(IRQ14, (uint32_t) irq14);
        idt_set_gate(IRQ15, (uint32_t) irq15);
}
