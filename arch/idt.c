#include <idt.h>
#include <stdint.h>
#include <lio.h>

idt_gate_t idt[256];
idt_reg_t idt_reg;

void idt_set_gate(int n, uint32_t handler)
{
        idt[n].low_offset = (uint16_t) handler;
        idt[n].sel = 0x08;
        idt[n].zero = 0;
        idt[n].flags = 0x8E;
        idt[n].high_offset = (uint16_t) (handler >> 16);
}

void idt_init()
{
        idt_reg.base = (uint32_t) &idt;
        idt_reg.limit = 256 * sizeof(idt_gate_t) - 1;

        asm volatile ("lidtl (%0)" : : "r" (&idt_reg));
}
