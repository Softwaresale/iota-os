
#include <idt.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <lio.h>
#include <isr.h>
#include <irq.h>

void isr_init()
{
        idt_set_gate(0,  (uint32_t) isr_zero_divide);
        idt_set_gate(1,  (uint32_t) isr_debug);
        idt_set_gate(2,  (uint32_t) isr_non_mask_int);
        idt_set_gate(3,  (uint32_t) isr_int3);
        idt_set_gate(4,  (uint32_t) isr_into);
        idt_set_gate(5,  (uint32_t) isr_out_bounds);
        idt_set_gate(6,  (uint32_t) isr_invalid_opcode);
        idt_set_gate(7,  (uint32_t) isr_coproc_not_available);
        idt_set_gate(8,  (uint32_t) isr_double_fault);
        idt_set_gate(9,  (uint32_t) isr_coproc_seg_overrun);

        idt_set_gate(10, (uint32_t) isr_bad_tss);
        idt_set_gate(11, (uint32_t) isr_no_seg);
        idt_set_gate(12, (uint32_t) isr_stack_fault);
        idt_set_gate(13, (uint32_t) isr_gen_protect);
        idt_set_gate(14, (uint32_t) isr_page_fault);
        idt_set_gate(15, (uint32_t) isr_res15);
        idt_set_gate(16, (uint32_t) isr_float_pt);
        idt_set_gate(17, (uint32_t) isr_align_check);
        idt_set_gate(18, (uint32_t) isr_machine_check);
        idt_set_gate(19, (uint32_t) isr_res19);
        idt_set_gate(20, (uint32_t) isr_res20);
        idt_set_gate(21, (uint32_t) isr_res21);
        idt_set_gate(22, (uint32_t) isr_res22);
        idt_set_gate(23, (uint32_t) isr_res23);
        idt_set_gate(24, (uint32_t) isr_res24);
        idt_set_gate(25, (uint32_t) isr_res25);
        idt_set_gate(26, (uint32_t) isr_res26);
        idt_set_gate(27, (uint32_t) isr_res27);
        idt_set_gate(28, (uint32_t) isr_res28);
        idt_set_gate(29, (uint32_t) isr_res29);
        idt_set_gate(30, (uint32_t) isr_res30);
        idt_set_gate(31, (uint32_t) isr_res31);

        irq_init();

        idt_init();
}

static const char *exception_messages[] = {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",

    "Double Fault",
    "Coprocessor Segment Overrun",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Unknown Interrupt",

    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",

    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

void isr_handler(regs_t reg)
{
        printk("Recieved int: %d\n%s\n", reg.int_no, exception_messages[reg.int_no]);
}

isr_t handlers[256];

void isr_reg_handler(uint8_t n, isr_t handler)
{
        handlers[n] = handler;
}


void irq_handler(regs_t r)
{
        if (r.int_no >= 40)
                outb(0xA0, 0x20);
        outb(0x20, 0x20);

        if (handlers[r.int_no] != 0){
                isr_t handler = handlers[r.int_no];
                handler(r);
        }
}
