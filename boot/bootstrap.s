
    /* Multiboot header */
    .set ALIGN, 1<<0               // alignment
    .set MEMINFO, 1<<1             // memory map
    .set FLAGS, ALIGN | MEMINFO    // multiboot flag field
    .set MAGIC, 0x1BADB002         // magic number so that bootloader can find mb
    .set CHECKSUM, -(MAGIC+FLAGS)  // checksum to prove multiboot

    .section .multiboot
    .align 4
    .long MAGIC
    .long FLAGS
    .long CHECKSUM

    /* Declare the stack */
    .section .bss
    .align 16
stack_bottom:
    .skip 16384 # 16 KiB
stack_top:

    /* Declare kernel entry point as start */
    .section .text
    .global _start
    .type _start @ function

_start:

    /* set stack */
    mov $stack_top, %esp

    /* Configuration here */

    /* High level kernel */
    push %ebx // load multiboot into EBX
    call kmain

    /* Infinate loop when system is done */
    cli
1:  hlt
    jmp 1b

    .size _start, . - _start
