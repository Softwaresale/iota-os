
	.text
	.global cpuid_getfeats

	/* Push 1 into eax, call cpuid, and return a pointer to the feature
	   bit map to the user */
cpuid_getfeats:

	mov $0x1, %eax

	cpuid /* Call cpuid */

	mov %edx, %eax

	ret
