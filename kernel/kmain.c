#include <vga_tty.h>
#include <stdio.h>
#include <pic.h>
#include <cpuid.h>
#include <apic.h>
#include <idt.h>
#include <isr.h>
#include <irq.h>
#include <asciiart.h>
#include <multiboot.h>
#include <paging.h>
#include <ram.h>
#include <pmm.h>
#include <irq.h>
#include <liballoc.h>

static void init(multiboot_info_t *);
static void check_mmap(multiboot_info_t *);

static ram_t kram;
extern unsigned int end; // Defined in linker.ld
static uint32_t *kern_end;

void dummy_entry(void)
{ }

void kmain(multiboot_info_t *mb_info, uint32_t magic)
{
     // Get end of kernel
     kern_end = (uint32_t *) &end;

     init(mb_info);
     printk("All systems initilized\n");

     printk(MAIN_LOGO);

     char *ptr1 = kmalloc(8);
     char *ptr2 = kmalloc(8);
     printk("Pointer1 at: 0x%x\n", &ptr1);
     printk("Pointer2 at: 0x%x\n", &ptr2);
     kfree(ptr1);
     kfree(ptr2);
}


static void init(multiboot_info_t *mb_info)
{
     vga_tty_init();
     printk("TTY on VGA ... [DONE]\n");

     if (!mb_info) {
          printk("[ERROR] Multiboot info not present\n");
     } else {
          printk("Multiboot located at 0x%x\n", &mb_info);
     }

     int ret = apic_detect();
     if (ret) {
          printk("[APIC] Detected\n");
          pic_disable();
          printk("[APIC] PIC has been disabled\n");
     } else {
          printk("[APIC] Undetected\n");
          printk("[APIC] Falling back on PIC\n");
     }

     isr_init();
     printk("IDT ... [DONE]\n");
     printk("ISR ... [DONE]\n");
     printk("IRQ ... [DONE]\n");

     check_mmap(mb_info);

     pmm_init(kram.size);
     printk("PMM ... [DONE]\n");

     paging_init();
     printk("Paging ... [DONE]\n");
}

static void check_mmap(multiboot_info_t *mbinfo)
{
     if (mbinfo->flags & 1) {
          kram = ram_get_usable(mbinfo);
          kram.start_addr = ((uint32_t) kern_end) + 0x1;
          ram_print(kram);
     } else {
          printk("[check_mmap] Flags not set\n");
     }
}
