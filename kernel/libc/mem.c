
#include <mem.h>
#include <stdint.h>

void *memcpy(void *dest, const void *src, int count)
{
     int i;
     const char *sp = (const char *) src;
     char       *dp = (char *)       dest;

     for (i = 0; i < count; i++)
          dp[i] = sp[i];

     return dest;
}

void *memset(void *dest, char val, int count)
{
     int i;
     char *dp = (char *) dest;

     for (i = 0; i < count; i++)
          dp[i] = val;

     return dest;
}

uint16_t *memsetw(uint16_t *dest, uint16_t val, int count)
{
     for (int i = 0; i < count; i++)
          dest[i] = val;

     return dest;
}
