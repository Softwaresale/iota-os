
#include <mem.h>
#include <stdint.h>
#include <string.h>

int strlen(const char *str)
{
        int i = 0;
        while (str[i] != '\0') ++i;
        return i;
}

int strcmp(const char *str1, const char *str2)
{
        int i;
        for (i = 0; str1[i] == str2[i]; i++)
                if (str1[i] == '\0') return 0;
        return str1[i] - str2[i];
}

int strcat(char *buf, char c)
{
        int len = strlen(buf);
        buf[len] = c;
        buf[++len] = '\0';

        return 0; // don't know why I did this?
}

char *strcpy(char *parent, char *add)
{
        int len = strlen(parent);
        int total_len = len + strlen(add);
        int i;

        for (i = 0; i < total_len; i++)
                parent[i + len] = add[i];

        parent[i + len + 1] = '\0';

        return parent;
}

void itoa(char *buf, unsigned long int n, int base)
{
        unsigned long int tmp;

        int i, j;

        tmp = n;
        i = 0;

        do {
                tmp = n % base;
                buf[i++] = (tmp < 10) ? (tmp + '0') : (tmp + 'a' - 10);
        } while (n /= base);
        buf[i--] = 0;

        for (j = 0; j < i; j++, i--) {
                tmp = buf[j];
                buf[j] = buf[i];
                buf[i] = tmp;
        }
}

static int isspace(char c)
{
        return c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r';
}

int atoi(char *str)
{
        int result = 0;
        unsigned int digit;
        int sign;

        while(isspace(*str))
                str += 1;

        if (*str == '-'){
                sign = 1;
                str += 1;
        } else {
                sign = 0;
                if (*str == '+')
                        str += 1;
        }

        for (;; str += 1) {
                digit = *str - '0';
                if (digit > 9)
                        break;

                result = (10 * result) + digit;
        }

        if (sign)
                return -result;

        return result;
}
