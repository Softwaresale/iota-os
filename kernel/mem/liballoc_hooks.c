
#include <stdint.h>
#include <stddef.h>
#include <liballoc.h>
#include <paging.h>
#include <pmm.h>

static const int __unlocked = 1;
static const int __locked   = 0;
static volatile unsigned char lock;

int liballoc_lock(void)
{
     if (lock != __locked) {
          lock = __locked;
          return 0;
     }
     return 1;
}

int liballoc_unlock(void)
{
     if (lock != __unlocked) {
          lock = __unlocked;
          return 0;
     }
     return 1;
}

void *liballoc_alloc(size_t size)
{
     return (void *) pmm_alloc_blocks(size);
}

int liballoc_free(void *ptr, size_t size)
{
     pmm_free_blocks((uint32_t) ptr, size);
     return 0;
}
