
    .text
    .global paging_load_dir
    .global paging_enable

paging_load_dir:
    push %ebp
    mov %esp, %ebp
    mov 8(%ebp), %eax
    mov %eax, %cr3
    mov %ebp, %esp
    pop %ebp
    ret

paging_enable:
    push %ebp
    mov %esp, %ebp
    mov %cr0, %eax
    or $0x80000000, %eax
    mov %eax, %cr0
    mov %ebp, %esp
    pop %ebp
    ret
