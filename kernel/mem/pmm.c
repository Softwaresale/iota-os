
#include <pmm.h>
#include <stdint.h>
#include <stddef.h>
#include <ram.h>
#include <mem.h>

extern unsigned int end; // defined in kmain.c
static uint8_t *bitmap = (uint8_t *) &end;
static uint8_t *mem_start;
static uint32_t total_blocks;
static uint32_t bitmap_size;

static inline void pmm_set_bit(unsigned int bit) {
     bitmap[bit / 8] |= (1 << (bit % 8));
}

static inline void pmm_unset_bit(unsigned int bit) {
     bitmap[bit / 8] &= ~(1 << (bit % 8));
}

static inline int pmm_test_bit(unsigned int bit) {
     return (bitmap[bit / 8] >> (bit % 8)) & 0x1;
}

static uint32_t __first_free_block(void);
static void     __find_free_blocks(size_t , uint32_t *);

void pmm_init(size_t size)
{
     total_blocks = size / PMM_BLOCK_SIZE;
     bitmap_size = total_blocks / 8;

     if (bitmap_size * 8 < total_blocks)
          bitmap_size++;

     memset(bitmap, 0, bitmap_size);
     mem_start =
          (uint8_t *) PMM_ALIGN_BLOCK(((uint32_t)(bitmap + bitmap_size)));
}

uint32_t pmm_alloc_block(void)
{
     uint32_t free_block = __first_free_block();
     pmm_set_bit(free_block);
     return free_block;
}

uint32_t pmm_alloc_blocks(size_t num)
{
     uint32_t blocks[num];
     __find_free_blocks(num, blocks);

     if (blocks[0] == 0)
          return 0;

     return *blocks;
}

void pmm_free_block(uint32_t block)
{
     pmm_unset_bit(block);
}

void pmm_free_blocks(uint32_t addr, size_t num)
{
     // Increment for each block
     // address should increase each block size
     for (int i = 0; i < num; i++, addr += 4096) {
          pmm_unset_bit(addr);
     }
}

static uint32_t __first_free_block(void)
{
     uint32_t i;
     for (i = 0; i < total_blocks; i++) {
          if (!pmm_test_bit(i))
               return i;
     }
     return 0;
}

static void __find_free_blocks(size_t num, uint32_t *out)
{
     unsigned char ctr = 0;

     // Iterate to find a first free block
     for (uint32_t i = 0; i < total_blocks; i++) {
          if (!pmm_test_bit(i)) {

               // If not allocated, then add it to the output buffer
               out[ctr++] = i;

               // Look at the ones next to it
               for (uint32_t j = i + 1; i < num - 1; j++) {
                    if (!pmm_test_bit(j)) {

                         if (ctr == num)
                              return; // We have found everything

                         // If this one is free, add it
                         out[ctr++] = j;

                    } else {
                         // If this one is not free, then we have to look for a new block
                         ctr = 0;
                         break;
                    } // else
               } // for
          } // if
     } // for
}
