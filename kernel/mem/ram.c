
#include <ram.h>
#include <multiboot.h>
#include <stdint.h>
#include <stdio.h>

extern unsigned int end;

ram_t ram_get_usable(multiboot_info_t *mb)
{
     printk("[Ram detection] Starting...\n");
     ram_t ram;

     multiboot_memory_map_t *mmap = (multiboot_memory_map_t*) mb->mmap_addr;

     uint32_t *kern_end = (uint32_t*) &end;

     int block_num = 0;
     while (mmap < (mb->mmap_addr + mb->mmap_length)){

          if (mmap->type == 1){
               if ((mmap->addr + mmap->len) > kern_end)
               {
                    ram.ram_num = block_num;
                    ram.start_addr = kern_end + 1;
                    ram.end_addr = mmap->addr + mmap->len;
                    ram.size = mmap->len - ((uint32_t) kern_end + 1 - mmap->addr);
                    break;
               }

          }

          mmap = (multiboot_memory_map_t*)
               ((unsigned int) mmap + mmap->size + sizeof(mmap->size));
          block_num++;
     }

     return ram;
}

void ram_print(ram_t ram)
{
     uint32_t *kern_end = (uint32_t*) &end;

     printk("Kernel end: %d\n\n", kern_end);
     printk("Ram:\n");
     printk("\tRam block number: %d\n", ram.ram_num);
     printk("\tStart Address:    0x%x\n", ram.start_addr);
#if 0
     printk("\tEnd Address:      0x%x\n", ram.end_addr);
#endif
     printk("\tSize:             0x%x\n\n", ram.size);
}
