
#include <paging.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <mem.h>
#include <lio.h>
#include <pmm.h>
#include <isr.h>

static page_dir_t *kpagedir;

static inline pt_entry_t *__ptable_lookup(page_tab_t *tab, vaddr_t addr)
{
     if (tab)
          return &tab->pentries[PAGE_TAB_IDX(addr)];
     return NULL;
}

static inline pd_entry_t *__pdir_lookup_entry(page_dir_t *dir, vaddr_t addr)
{
     if (dir)
          return &dir->entries[PAGE_TAB_IDX(addr)];
     return NULL;
}

static inline int __pdir_switch_dir(page_dir_t *dir)
{
     if (!dir)
          return -1;

     kpagedir = dir;
     paging_load_dir((uint32_t *) kpagedir);
     return 0;
}

void page_fault_handler(regs_t *);

void paging_init(void)
{
     page_tab_t *tab, *tab2;
     tab = (page_tab_t *) pmm_alloc_block();
     if (!tab)
          return;

     tab2 = (page_tab_t *) pmm_alloc_block();
     if (!tab2)
          return;

     memset(tab,  0, sizeof(page_tab_t));
     memset(tab2, 0, sizeof(page_tab_t));

     int i, frame, virt;

     for (i = 0, frame = 0x0, virt=0x00000000;
          i < 1024;
          i++, frame += 4096, virt += 4096) {

          pt_entry_t page;
          page.present = 1;
          page.frame_addr = frame;
          tab2->pentries[PAGE_TAB_IDX(virt)] = page;
     }

     for (i = 0, frame = 0x100000, virt=0xc0000000;
          i < 1024;
          i++, frame += 4096, virt += 4096) {

          pt_entry_t page;
          page.present = 1;
          page.frame_addr = frame;

          tab->pentries[PAGE_TAB_IDX(virt)] = page;
     }

     page_dir_t *dir = (page_dir_t *) pmm_alloc_block();
     if (!dir)
          return;

     memset(dir, 0, sizeof(page_dir_t));

     pd_entry_t *entry = &dir->entries[PAGE_DIR_IDX(0xc0000000)];
     entry->present = 1;
     entry->rw = 1;
     entry->page_table_base = (uint32_t) tab;

     pd_entry_t *entry2 = &dir->entries[PAGE_DIR_IDX(0x00000000)];
     entry2->present = 1;
     entry2->rw = 1;
     entry2->page_table_base = (uint32_t) tab2;

     kpagedir = dir;

     isr_reg_handler(14, page_fault_handler);

     paging_load_dir((uint32_t *) dir);
     paging_enable();
}

int paging_alloc_page(pt_entry_t *entry)
{
     void *frame = (void *) pmm_alloc_block();
     if (!frame)
          return -1;

     entry->frame_addr = (uint32_t) frame;
     entry->present = 1;

     return 0;
}

void paging_free_page(pt_entry_t *entry)
{
     if (entry->frame_addr)
          pmm_free_block(entry->frame_addr);
     entry->present = 0;
}

void paging_map_page(void *phys, void *virt)
{
     pd_entry_t *pde = &kpagedir->entries[PAGE_DIR_IDX((uint32_t) virt)];

     if (!pde->present) {
          // Allocate new block
          page_tab_t *tab = (page_tab_t *) pmm_alloc_block();
          if (!tab)
               return;

          // Clear it and set it to a new entry
          memset(tab, 0, sizeof(page_tab_t));
          pd_entry_t *new_pde =
               &kpagedir->entries[PAGE_DIR_IDX((uint32_t) virt)];

          new_pde->present = 1;
          new_pde->rw = 1;
          new_pde->page_table_base = tab;
     }

     page_tab_t *table = (page_tab_t *) PAGE_PHYS_ADDR(pde);
     pt_entry_t *page = &table->pentries[PAGE_TAB_IDX((uint32_t) virt)];
     page->frame_addr = (uint32_t) phys;
     page->present = 1;
}

void page_fault_handler(regs_t *reg)
{
     asm volatile ("sti");
     printk("PAGE FAULT:\n");

     uint32_t fault_addr;
     asm volatile ("mov %%cr2, %0" : "=r" (fault_addr));
     printk("Faulting address: 0x%x\n", fault_addr);
}
