

# tools
GCC=i686-elf-gcc
LD=i686-elf-ld
AS=i686-elf-as
NASM=nasm

# flags
CFLAGS = -std=gnu99 -ffreestanding -O2 -Wall -Wextra -I include/
LDFLAGS = -T linker.ld -o iota.bin -melf_i386
NFLAGS = -felf32

# Code bases
ARCH_SRC=$(wildcard arch/*.c)
BOOT_SRC=$(wildcard boot/*.s)
DRIVERS_SRC=$(wildcard drivers/*.c)
KERNEL_SRC=$(wildcard kernel/*.c kernel/libk/*.c kernel/libc/*.c kernel/mem/*.c)

ARCH_S=$(wildcard arch/*.s)
DRIVERS_S=$(wildcard drivers/*.s)
KERNEL_S=$(wildcard kernel/*.s kernel/libc/*.s kernel/libk/*.s kernel/mem/*.s)
KERNEL_ASM=$(wildcard kernel/*.asm kernel/libc/*.asm kernel/libk/*.asm kernel/mem/*.asm)

ARCH_OBJ=$(ARCH_SRC:.c=.o)
BOOT_OBJ=$(BOOT_SRC:.s=.o)
DRIVERS_OBJ=$(DRIVERS_SRC:.c=.o)
KERNEL_OBJ=$(KERNEL_SRC:.c=.o)

ARCH_SOBJ=$(ARCH_S:.s=.o)
KERNEL_SOBJ=$(KERNEL_S:.s=.o)
KERNEL_ASMOBJ=$(KERNEL_ASM:.asm=.o)
DRIVERS_SOBJ=$(DRIVERS_S:.s=.o)

# Targets

all: arch boot drivers include kernel
	$(LD) $(LDFLAGS) $(ARCH_OBJ) $(ARCH_SOBJ) $(BOOT_OBJ) $(DRIVERS_OBJ) $(KERNEL_OBJ) $(KERNEL_ASMOBJ) $(KERNEL_SOBJ) $(DRIVERS_SOBJ)

arch: $(ARCH_OBJ) $(ARCH_SOBJ)

boot: $(BOOT_OBJ)

drivers: $(DRIVERS_OBJ) $(DRIVERS_SOBJ)

kernel: $(KERNEL_OBJ) $(KERNEL_ASMOBJ) $(KERNEL_SOBJ)

clean:
	rm $(KERNEL_OBJ)
	rm $(KERNEL_ASMOBJ)
	rm $(DRIVERS_OBJ)
	rm $(DRIVERS_SOBJ)
	rm $(BOOT_OBJ)
#	rm $(ARCH_OBJ)
	rm iota.bin
	rm isodir/boot/iota.bin
	rm iota.iso

iso: iota.bin
	cp iota.bin isodir/boot/iota.bin
	grub2-mkrescue -o iota.iso isodir

run: iso 
	/usr/bin/qemu-system-x86_64 \
    -monitor stdio \
    -machine accel=kvm \
    -m 1024 \
    -cdrom /home/charlie/Programming/iota-os/iota.iso \
    -hda /home/charlie/.aqemu/iota_os_HDA.img \
    -boot once=d,menu=on \
    -net none \
    -rtc base=localtime \
    -name "iota-os"

# file compilation rules
%.o: %.c
	$(GCC) $(CFLAGS) -c $< -o $@

%.o: %.s
	$(AS) $< -o $@

%.o: %.asm
	$(NASM) $(NFLAGS) $< -o $@
