
#ifndef STDIO_H
#define STDIO_H

#include <vga_tty.h>
#include <stdint.h>
#include <stdarg.h>

void vsprintf(char *, void (*printchar)(const char), const char*, va_list);

int printk(const char *, ...);

// int printf(const char *str, ...);

#endif // STDIO_H
