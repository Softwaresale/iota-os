
#ifndef PMM_H
# define PMM_H

#include <stdint.h>
#include <ram.h>
#include <stddef.h>

#define PMM_BLOCK_SIZE 4096
#define PMM_ALIGN_BLOCK(addr) (((addr) & 0xFFFFF000) + 0x1000)

void      pmm_init(size_t);
uint32_t  pmm_alloc_block(void);
uint32_t  pmm_alloc_blocks(size_t);
void      pmm_free_block(uint32_t);
void      pmm_free_blocks(uint32_t, size_t);

#endif // PMM_H
