
#ifndef PIC_H
#define PIC_H

#include <stdint.h>

/* I/O Ports for PIC */
#define PIC_MASTER_CMD  0x0020
#define PIC_MASTER_DATA 0x0021
#define PIC_SLAVE_CMD   0x00A0
#define PIC_SLAVE_DATA  0x00A1

/* ICW values */
#define ICW1_ICW4      0x01 // ICW4 not needed
#define ICW1_SINGLE    0x02 // single/cascade mode
#define ICW1_INTERVAL4 0x04 // call address interval 4 (8)
#define ICW1_LEVEl     0x08 // level triggered
#define ICW1_INIT      0x10 // initilize

#define ICW4_8086       0x01 // 8086 mode
#define ICW4_AUTO       0x02 // auto EOI mode
#define ICW4_BUF_SLAVE  0x08 // buffered slave
#define ICW4_BUF_MASTER 0x0C // buffered master
#define ICW4_SFNM       0x10 // special fully nested (not)

/* PIC commands */
#define PIC_EOI      0x20
#define PIC_INIT     0x11
#define PIC_READ_IRR 0x0a
#define PIC_READ_ISR 0x0b

/* Send end of interrupt to IRQ line */
void pic_send_eoi(uint8_t);

/* Remap the pic, used for initilization */
void pic_remap(uint32_t, uint32_t);

/* set a mask on an IRQ line */
void pic_irq_set_mask(uint8_t);

/* Clear a mask on IRQ line */
void pic_irq_clear_mask(uint8_t);

uint16_t pic_get_irr(void);

uint16_t pic_get_isr(void);

void pic_disable();

#endif // PIC_H
