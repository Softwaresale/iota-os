
#ifndef CPUID_H
#define CPUID_H

#include <stdint.h>

/* Both defined in cpuid.s */
extern int cpuid_detect(void);
extern uint32_t cpuid_getfeats(void); // returns pointer to bitmap

#endif // CPUID_H
