
#ifndef RAM_H
#define RAM_H

#include <stdint.h>
#include <multiboot.h>

typedef struct _ram
{
        uintptr_t start_addr; // start of ram block
        uintptr_t end_addr;   // end of ram block
        uint64_t  size;       // total size of ram block
        uint8_t   ram_num;    // which block of ram the block represents
} ram_t;

ram_t ram_get_usable(multiboot_info_t*);
void  ram_print(ram_t);

#endif // RAM_H
