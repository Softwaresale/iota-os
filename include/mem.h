
#ifndef MEM_H
#define MEM_H

#include <stdint.h>
#include <stddef.h>

#ifndef NULL
# define NULL 0
#endif

void *memcpy(void *, const void*, int);

void *memset(void *, char, int);

uint16_t *memsetw(uint16_t *, uint16_t, int);

#endif // MEM_H
