
#ifndef STRING_H
#define STRING_H

#include <mem.h>
#include <stdint.h>

int strlen(const char *);

int strcmp(const char *, const char *);

int strcat(char *, char);

// char *strdup(const char *);

char *strcpy(char *, char *);

void itoa(char *, unsigned long int, int);

int atoi(char *);

#endif // STRING_H
