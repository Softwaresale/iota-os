
#ifndef IDT_H
#define IDT_H

#include <stdint.h>

/* definition of each handler */
typedef struct _idt_gate
{
        uint16_t  low_offset;   // lower 16 bits of handler function addr
        uint16_t  sel;          // kernel segment selector
        uint8_t   zero;         // always zero
        uint8_t   flags;        // 7: enabled, 6-5: privlege 4: 0 3-0: 32 bit interrupt gate
        uint16_t  high_offset;  // upper half of handler function
}__attribute__((packed)) idt_gate_t;

/* Array of interrupt handlers */
typedef struct _idt_reg
{
        uint16_t limit;
        uint8_t  base;
}__attribute__((packed)) idt_reg_t;

void idt_set_gate(int, uint32_t);
void idt_init();

#endif // IDT_H
