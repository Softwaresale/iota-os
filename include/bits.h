#ifndef BITS_H
#define BITS_H

#include <stdint.h>

/*
 * Used for handling bit sets. These macros are used
 * for setting a specific bit withing a value at offset
 */

#define SET_BIT(val, off) val |= (1 << off)
#define CLEAR_BIT(val, off) val &= ~(1 << off)
#define TOGGLE_BIT(val, off) val ^= (1 << off)
#define BIT_IS_SET(val, off) (val & (1 << off))
#define GET_BIT(val, off) (val & (val << off)) >> off

#endif
