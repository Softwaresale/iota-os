
#ifndef ISR_H
#define ISR_H

#include <stdint.h>

extern void isr_zero_divide();
extern void isr_debug();
extern void isr_non_mask_int();
extern void isr_int3();
extern void isr_into();
extern void isr_out_bounds();
extern void isr_invalid_opcode();
extern void isr_coproc_not_available();
extern void isr_double_fault();
extern void isr_coproc_seg_overrun();
extern void isr_bad_tss();
extern void isr_no_seg();
extern void isr_stack_fault();
extern void isr_gen_protect();
extern void isr_page_fault();
extern void isr_res15();
extern void isr_float_pt();
extern void isr_align_check();
extern void isr_machine_check();
extern void isr_res19();
extern void isr_res20();
extern void isr_res21();
extern void isr_res21();
extern void isr_res22();
extern void isr_res23();
extern void isr_res24();
extern void isr_res25();
extern void isr_res26();
extern void isr_res27();
extern void isr_res28();
extern void isr_res29();
extern void isr_res30();
extern void isr_res31();

typedef struct
{
        uint32_t ds;
        uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
        uint32_t int_no, err_code;
        uint32_t eip, cs, eflags, useresp, ss;
} regs_t;

void isr_init();
void isr_handler(regs_t);

typedef void (*isr_t)(regs_t);

void isr_reg_handler(uint8_t, isr_t);

#endif // ISR_H
