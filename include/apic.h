
#ifndef APIC_H
#define APIC_H

#define APIC_ENABLED

#define APIC_BASE_MSR 0x1B
#define APIC_MSR_ENABLED 0x800

/* Set preprocessor macro */
int apic_detect(void);

#endif // APIC_H
