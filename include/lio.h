
#ifndef LIO_H
#define LIO_H

#include <stdint.h>

/* Output data to port */
void outb(uint16_t, uint8_t);
void outw(uint16_t, uint16_t);
void outl(uint16_t, uint32_t);

/* Read data from port */
uint8_t  inb(uint16_t);
uint16_t inw(uint16_t);
uint32_t inl(uint16_t);

void io_wait();

#endif // LIO_H
