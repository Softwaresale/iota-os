
#ifndef PAGING_H
#define PAGING_H

#include <stdint.h>

#define PAGE_DIR_IDX(x)   (((x) >> 22) & 0x3ff)
#define PAGE_TAB_IDX(x)   (((x) >> 12) & 0x3ff)
#define PAGE_PHYS_ADDR(x) (*((uint32_t *) x) & ~0xfff)
#define PAGE_SIZE         4096

typedef struct _pd_entry
{
     uint8_t  present         : 1;
     uint8_t  rw              : 1;
     uint8_t  premission      : 1;
     uint8_t  write_through   : 1;
     uint8_t  cache_disabled  : 1;
     uint8_t  accessed        : 1;
     uint8_t  reserved        : 1;
     uint8_t  page_size       : 1;
     uint8_t  ignored         : 1;
     uint8_t  available       : 2;
     uint32_t page_table_base : 21;
}__attribute__((aligned(4096))) pd_entry_t;

typedef struct _pd
{
     pd_entry_t entries[1024];
} __attribute__((aligned(4096))) page_dir_t;

/*
typedef struct _page_tab
{
     uint8_t present         : 1;
     uint8_t rw              : 1;
     uint8_t premission      : 1;
     uint8_t write_through   : 1;
     uint8_t cache_disabled  : 1;
     uint8_t accessed        : 1;
     uint8_t dirty           : 1;
     uint8_t global          : 1;
     uint8_t available       : 3;
     uint32_t phys_page_addr : 21;
}__attribute__((aligned(4096))) page_tab_t;
*/

typedef struct pt_entry
{
     uint8_t present     : 1;
     uint8_t rw          : 1;
     uint8_t mode        : 1;
     uint8_t reserved    : 2;
     uint8_t access      : 1;
     uint8_t dirty       : 1;
     uint8_t reserved2   : 2;
     uint8_t avail       : 2;
     uint32_t frame_addr : 21;
}__attribute__((aligned(4096))) pt_entry_t;

typedef struct pt
{
     pt_entry_t pentries[1024];
} __attribute__((aligned(4096))) page_tab_t;

/* Virtual address type */
typedef uint32_t vaddr_t;

/* Physical address type */
typedef uint32_t paddr_t;

extern void paging_load_dir(uint32_t *);
extern void paging_enable(void);

void paging_init(void);

int paging_alloc_page(pt_entry_t *);
void paging_free_page(pt_entry_t *entry);
void paging_map_page(void *phys, void *virt);

#endif // PAGING_H
