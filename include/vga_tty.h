
#ifndef VGA_TTY_H
#define VGA_TTY_H

#include <stdint.h>

#define VGA_WIDTH  80
#define VGA_HEIGHT 25

#define VGA_TTY_CTRL_REG 0x3d4
#define VGA_TTY_DATA_REG 0x3d5

#define GET_OFFSET(x, y) (y * VGA_WIDTH + x)

enum vga_color {

        VGA_COLOR_BLACK      = 0,
        VGA_COLOR_BLUE       = 1,
        VGA_COLOR_GREEN      = 2,
        VGA_COLOR_RED        = 4,
        VGA_COLOR_LIGHT_GRAY = 8,
        VGA_COLOR_WHITE      = 15
};

void vga_tty_clear();

void vga_tty_putc(const char);

void vga_tty_puts(const char *str);

void vga_tty_set_color(uint8_t);

void vga_tty_init();

#endif // VGA_TTY_H
