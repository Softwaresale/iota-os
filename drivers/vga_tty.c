
#include <vga_tty.h>
#include <mem.h>
#include <string.h>
#include <lio.h>

static unsigned short *buf; // actual text buffer
static int x = 0;
static int y = 0;
static uint8_t color;

static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg)
{
     return fg | bg << 4;
}

static inline uint16_t vga_entry(unsigned char c, uint8_t color)
{
     return (uint16_t) c | (uint16_t) color << 8;
}

static void vga_tty_move_cursor()
{
     unsigned short index = GET_OFFSET(x, y);

     outb(VGA_TTY_CTRL_REG, 14);
     outb(VGA_TTY_DATA_REG, index >> 8);
     outb(VGA_TTY_CTRL_REG, 15);
     outb(VGA_TTY_DATA_REG, index);
}

void vga_tty_clear()
{
     for (int i = 0; i < VGA_HEIGHT; i++)
          memsetw(buf + i * VGA_WIDTH, vga_entry(' ', color), VGA_WIDTH);

     x = 0;
     y = 0;
     vga_tty_move_cursor();
}

static void vga_tty_scroll()
{
     if (y >= VGA_HEIGHT) {
          unsigned short tmp = y - VGA_HEIGHT + 1;

          memcpy(buf, buf + tmp * VGA_WIDTH, (VGA_HEIGHT - tmp) * VGA_WIDTH * 2);
          memsetw(buf + (VGA_HEIGHT - tmp) * VGA_WIDTH,
                  vga_entry(' ', color),
                  VGA_WIDTH);

          y = VGA_HEIGHT - 1; // reset y val
     }
}

static void vga_tty_printchar(const char ch)
{
     unsigned short *where;

     if (ch == 0x08){
          if (x > 0)
               x--;

          where = buf + GET_OFFSET(x, y);
          *where = vga_entry(ch, color);
     }

     if (ch == 0x09)
          x = (x + 8) & ~(8-1);

     if (ch == '\r')
          x = 0;

     if (ch == '\n'){
          y++;
          x = 0;
     }

     if (ch >= ' '){
          where = buf + GET_OFFSET(x, y);
          *where = vga_entry(ch, color);
          x++;
     }

     if (x >= VGA_WIDTH){
          x = 0;
          y++;
     }

     vga_tty_scroll();
     vga_tty_move_cursor();
}

void vga_tty_putc(const char c)
{
     vga_tty_printchar(c);
}

void vga_tty_puts(const char *str)
{
     int len = strlen(str);

     for (int i = 0; i < len; i++)
          vga_tty_printchar(str[i]);
}

void vga_tty_set_color(uint8_t c)
{
     color = c;
}

void vga_tty_init()
{
     x = 0;
     y = 0;
     color = vga_entry_color(VGA_COLOR_BLUE, VGA_COLOR_WHITE);
     buf = (unsigned short *) 0xB8000;

     vga_tty_clear();
}
