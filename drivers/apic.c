#include <stdint.h>
#include <apic.h>
#include <pic.h>
#include <stdio.h>
#include <lio.h>
#include <bits.h>
#include <cpuid.h>

int apic_detect()
{
        uint32_t feat_bm = cpuid_getfeats(); // git the bitmap of features

        if (feat_bm & (1 << 9)){
                return 1;
        } else {
                #undef APIC_ENABLED
                return 0;
        }
}
