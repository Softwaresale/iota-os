
#include <pic.h>
#include <lio.h>
#include <stdint.h>

void pic_send_eoi(uint8_t irq)
{
        if (irq >= 8)
                outb(PIC_SLAVE_CMD, PIC_EOI);
        else
                outb(PIC_MASTER_CMD, PIC_EOI);
}

/* off1 -- master vector offset (offset -- offset+7)
   off2 -- same as master, but for slave */
void pic_remap(uint32_t off1, uint32_t off2)
{
        uint8_t master_mask = inb(PIC_MASTER_DATA);
        uint8_t slave_mask  = inb(PIC_SLAVE_DATA);

        outb(PIC_MASTER_CMD, ICW1_INIT+ICW1_ICW4); // initilize master
        io_wait();
        outb(PIC_SLAVE_CMD, ICW1_INIT+ICW1_ICW4); // initilize slave
        io_wait();
        outb(PIC_MASTER_DATA, off1); // send master PIC nvector offset
        io_wait();
        outb(PIC_SLAVE_DATA, off2);  // send slave offset
        io_wait();
        outb(PIC_MASTER_DATA, 4);    // master PIC slave PIC at irq2
        io_wait();
        outb(PIC_SLAVE_DATA, 2);     // slave PIC is cascade
        io_wait();

        // write the mode to the PIC
        outb(PIC_MASTER_DATA, ICW4_8086);
        io_wait();
        outb(PIC_SLAVE_DATA, ICW4_8086);
        io_wait();

        // restore the previous masks
        outb(PIC_MASTER_DATA, master_mask);
        outb(PIC_SLAVE_DATA, slave_mask);
}

void pic_disable()
{
        // disable the PIC
        outb(0xa1, 0xff);
        outb(0x21, 0xff);
}

static uint16_t __get_irq_line_data(uint8_t *line)
{
        if (line < 8){
                return PIC_MASTER_DATA;
        } else {
                line -= 8;
                return PIC_SLAVE_DATA;
        }
}

void pic_set_mask(uint8_t irq)
{
        uint16_t port = __get_irq_line_data(&irq);

        // sets the apropriate port with the value of one
        uint8_t value = inb(port) | (1 << irq);

        // writes the value back
        outb(port, value);
}

void pic_unset_mask(uint8_t irq)
{
        uint16_t port = __get_irq_line_data(&irq);

        // toggles the bit off
        uint8_t value = inb(port) & ~(1 << irq);
        outb(port, value);
}

static uint16_t __get_irq_reg(int ocw3)
{
        outb(PIC_MASTER_CMD, ocw3);
        outb(PIC_SLAVE_CMD,  ocw3);
        return (inb(PIC_SLAVE_CMD) << 8) | inb(PIC_MASTER_CMD);
}

uint16_t pic_get_irr(void)
{
        return __get_irq_reg(PIC_READ_IRR);
}

uint16_t pic_get_isr(void)
{
        return __get_irq_reg(PIC_READ_ISR);
}
